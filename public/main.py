#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Programa Gerenciamento de Usuario

# Programado por: HacktheBit
# Versão python utilizada: python3
# importação dos módulos necessários para ao funcionamento do programa
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window
from kivy.uix.label import Label
from kivy.uix.screenmanager import ScreenManager, Screen
import sqlite3
import time
import datetime

class Gerenciador(ScreenManager):
    pass

class Menu(Screen):
	pass

class Entrar(Screen):
    def login(self):
        login = self.root.ids.login.text
        passwd = self.root.ids.passwd.text
        if login == 'admin' and passwd == 'admin':
            pass

    def on_pre_enter(self):
        Window.bind(on_keyboard=self.voltar)

    def voltar(self,window,key,*args):
        if key == 27:
            App.get_running_app().root.current = 'menu'
            return True

    def on_pre_leave(self):
        Window.unbind(on_keyboard=self.voltar)

"""
class DataBase():
    def __init__(self,id,user,keyword):
        self.id = 1
        self.user = 'admin'
        self.keyword = 'admin'


    def create_table():
        #cria banco de dados
        connection = sqlite3.connect('users.db')
        #para manipular a tabela
        c = connection.cursor()
        c.execute('CREATE TABLE IF NOT EXISTS clientes (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, user TEXT NOT NULL, keyword TEXT NOT NULL, datestamp TEXT NOT NULL);')
    create_table()

    #date = str(datetime.datetime.fromtimestamp(int(time.time())).strftime('%Y-%m-%d %H:%M:%S'))

    def dataentry(self,id,user,keyword,*args):
        #cria banco de dados
        connection = sqlite3.connect('users.db')
        #para manipular a tabela
        c = connection.cursor()
        self.id = id
        self.user = user
        self.keyword = keyword
        c.execute('INSERT INTO clientes (id, user, keyword) VALUES (?,?,?)', (id, user, keyword))
        #c.execute('INSERT INTO clientes (id, user, keyword) VALUES (?,?,?)', (id, user, keyword))
        connection.commit()
    dataentry()

    sql = 'SELECT * FROM dados WHERE keyword = ?'
    sql2 = 'SELECT * FROM dados WHERE keyword = ? and user = ?'
    def read_data(wordUsed):
        for row in c.execute(sql, (wordUsed,)):
            print(row)
        print('========')
        for row in c.execute(sql2, (wordUsed, 'admin',)):
            print(row)
    read_data('admin')
"""

class Entrou(BoxLayout):
    def __init__(self,text='',**kwargs):
        super(Entrou,self).__init__(**kwargs)
        self.box.label.text = text

class CriarUser(Screen):
	pass

class UserManager(App):
	def build(self):
		return Gerenciador()

UserManager().run()
